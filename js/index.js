$(document).ready(function() {    
    var indicators = $('.weir-range-input').parent().find('.weir-selected-range');
    $("#bw-range").slider({});    
    indicators.each(function(i, indicator) {      
        var val = $(indicator).parent().find('.weir-range-input').val().split(',').join(' - ') + ' %';      
        $(indicator).html(val);    
    });    
    $('.weir-range-input').on('change', function() {      
        var range = this.value.split(','),        
            indicator = $(this).parent().find('.weir-selected-range');        
        range = range.join(' - ') + ' %';        
        indicator.html(range);    
    });  
})