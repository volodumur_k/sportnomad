(function($) {
    'use strict';

    window.addEventListener('load', function() {
        $('#navbarSupportedContent').on('show.bs.collapse', function () {
            var headHeight = $('.header').get(0).clientHeight;

            // Show the backdrop
            $('<div class="modal-backdrop fade"></div>').css('margin-top', headHeight + 'px').appendTo(document.body);
        });

        $('#navbarSupportedContent').on('shown.bs.collapse', function () {
            $('.modal-backdrop').addClass('show');
        });

        $('#navbarSupportedContent').on('hide.bs.collapse', function () {
            $('.modal-backdrop').removeClass('show');
        });

        $('#navbarSupportedContent').on('hidden.bs.collapse', function () {
            // Remove backdrop
            $('.modal-backdrop').remove();
        });
    });

})(jQuery);
